let trainerObject = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends : {
		hoenn : ["May", "Max"],
		kanta : ["Brock", "Misty"],
	},
	talk : function(){
		console.log("Result of talk method");
		console.log("Pikachu! I choose you!");
	},
};

console.log(trainerObject);

console.log("Result of dot notation:");
console.log(trainerObject.name);

console.log("Result of square bracket notation:");
console.log(trainerObject['pokemon']);

console.log("Result of talk method");
trainerObject.talk();

function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name)
			console.log(target.name + " health is now reduced to " + (target.health - this.attack));
			target.health = target.health - this.attack;

		};
		this.faint = function(){
			console.log(this.name + ' fainted ');
		};
	};

	let pikachu = new Pokemon("Pikachu", 12);
	let Geodude = new Pokemon("Geodude", 8);
	let Mewtwo = new Pokemon("Mewtwo", 100);

	console.log(pikachu);
	console.log(Geodude);
	console.log(Mewtwo);

	Geodude.tackle(pikachu);

	console.log(pikachu);
	Mewtwo.tackle(Geodude);
	Geodude.faint();

	console.log(Geodude);