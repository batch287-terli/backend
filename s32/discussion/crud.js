let http = require('http');

// mock database
let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
]

http.createServer(function(request, response){
	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'});
		//console.log(directory);
		response.write(JSON.stringify(directory));
		//console.log(JSON.stringify(directory));
		response.end();
	}
	if(request.url == '/users' && request.method == 'POST'){
		let requestBody = '';

		request.on('data' , function(data){
			requestBody += data;

		});
		request.on('end' , function(){
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			};
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200,{'content-type': 'application/json'});
			response.write(JSON.stringify(newUser))
			response.end();

		});
	}

}).listen(4001);

console.log('running');

