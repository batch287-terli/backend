console.log("Hello World");


//	1. Create variables to store to the following user details:

let	firstName = "Tejaswi";
console.log("First Name:" + firstName);
let	lastName = "Terli";
console.log("Last Name:" +lastName);
let myAge = 18;
console.log("Age:" +myAge);

let	hobbies = ['humming' ,'travelling' , 'hanging out with frnds'];
console.log("Hobbies:")
console.log(hobbies);

let	workAddress = {
	hostelNumber: "H15",
	street: "Powai Lake",
	college: "IITB",
	city: "Mumbai",
	state: "Maharashtra"
}
console.log("Work Address:");
console.log(workAddress);

	let fullName = "Steve Rogers";
	console.log("My full name is:" + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ['Tony','Bruce','Thor','Natasha','Clint','Nick'];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let frndName = "Bucky Barnes";
	console.log("My bestfriend is: " + frndName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

