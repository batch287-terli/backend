const http = require('http');

const port = 4004;

const server = http.createServer((request, response) => {
	if(request.url == '/greeting'){
		response.writeHead(200,{'content-type':'text/plain'})
		response.end('Welcome to the server!This is currently running at local host:4004.')

	}else if (request.url == '/homepage'){
		response.writeHead(200,{'content-type': 'text/plain'})
		response.end('Welcome to the Homepage!This is current;y running at local host:4004.')

	}else{
		response.writeHead(404,{'content-type': 'text/plain'})
		response.end('page not available');
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`)


