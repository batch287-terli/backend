// Use the "require" directive to load Node.js modules
// A module is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (Node.js/Express.js application) communicate by exchanging individual messages.
	// The messages sent by the 'client', usually a web browser are called "requests"
	/// The messages sent by the 'server', as an answer are called "responses"
const http = require('http');

http.createServer(function (request, response){

	response.writeHead(200, {'Content-Type': 'text/plain'});

	response.end('Hellow Wurld?');
}).listen(4000)

console.log('Server is running at localhost:4000');