let getCube = Math.pow(2, 3);
	console.log(`The cube of 2 is ${getCube}`);

let address = ['258 Washington Ave NW' , 'California' , 90011];
	let [address1 , address2 , pincode] = address;
	console.log(`I live at ${address1}, ${address2} ${pincode}`);

let animal = {
	name : 'Lolong',
	type : 'crocodile',
	weightinKg : 1075,
	feet : 20,
	inches : 3,
};

	let {name , type , weightinKg , feet , inches} = animal;
	console.log(`${name} was a saltwater ${type}. He weighted at ${weightinKg} kgs with a measurement of ${feet} ft ${inches} in.`);

let numbers = [1,2,3,4,5];

	numbers.forEach((num) =>{
		console.log(num);
	});

let iteration = 0;

	let reducedNumber = numbers.reduce((x,y) =>{
		return x + y;
		iteration++;
	});

	console.log(reducedNumber);

class dog{
	constructor(name , age , breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myNewDog = new dog ('Frankie', '5', 'Miniature Dachshund');
	console.log(myNewDog);