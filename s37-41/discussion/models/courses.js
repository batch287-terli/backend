const mongoose = require("mongoose");

const courseScheme = new mongoose.Schema({
	name : {
		type : String;
		required : [true, "Course is required."]
	},
	description :{
		type : String,
		required:[true , "Description is required."]
	},
	price: {
		type : Number,
		required : [ true , "price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn :{
		type : Date,
		default : new Date()
	},
	enrollees :[
		{
			courseId : {
				type : String,
				required :[true, "UzserId is required."]
			},
			enrolledOn :{
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				required :[true, "UzserId is required."]
			},
		}
	]
})

module.exports = mongoose.model("Course" , courseScheme);